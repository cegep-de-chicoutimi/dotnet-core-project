﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ThirdWheel.Areas.Identity.Data;
using ThirdWheel.Migrations;
using ThirdWheel.Models;

[assembly: HostingStartup(typeof(ThirdWheel.Areas.Identity.IdentityHostingStartup))]
namespace ThirdWheel.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
                //services.AddDbContext<ThirdWheelContext>(options =>
                //    options.UseSqlServer(
                //        context.Configuration.GetConnectionString("ThirdWheelContextConnection")));

                services.AddDefaultIdentity<ThirdWheelUser>()
                    .AddEntityFrameworkStores<ThirdWheelContext>();
            });
        }
    }
}