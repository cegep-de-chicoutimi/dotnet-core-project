﻿using Microsoft.AspNetCore.Identity;

namespace ThirdWheel.Areas.Identity.Data
{
    // Add profile data for application users by adding properties to the ThirdWheelUser class
    public class ThirdWheelUser : IdentityUser
    {
    }
}
