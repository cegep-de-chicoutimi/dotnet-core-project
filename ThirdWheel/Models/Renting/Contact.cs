﻿using System.ComponentModel.DataAnnotations;
using ThirdWheel.Models.Primitives;

namespace ThirdWheel.Models.Renting
{
    public class Contact
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public Image Image { get; set; }

        public string PhoneNumber { get; set; }
    }
}
