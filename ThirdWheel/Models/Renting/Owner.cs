﻿using System.ComponentModel.DataAnnotations;

namespace ThirdWheel.Models.Renting
{
    public class Owner
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public Contact Contact { get; set; }
    }
}
