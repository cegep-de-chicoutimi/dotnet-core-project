﻿using System.Collections.Generic;
using ThirdWheel.Models.Primitives;

namespace ThirdWheel.Models.Renting
{
    public class Bike
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Image> Images { get; set; }
        public float Price { get; set; }
        public float Cost { get; set; }
        public Owner Owner { get; set; }
        public List<Booking> Booking { get; set; }
    }
}
