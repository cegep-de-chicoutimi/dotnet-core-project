﻿using System;
using System.ComponentModel.DataAnnotations;
using ThirdWheel.Areas.Identity.Data;

namespace ThirdWheel.Models.Renting
{
    public class Booking
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public ThirdWheelUser Client { get; set; }

        [Required]
        public Bike Bike { get; set; }

        [Required]
        public DateTime PickUpTime { get; set; }

        [Required]
        public DateTime DropTime { get; set; }
    }
}
