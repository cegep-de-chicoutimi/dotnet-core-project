﻿using System.ComponentModel.DataAnnotations;
using ThirdWheel.Models.Primitives;

namespace ThirdWheel.Models.Destinations
{
    public class Site
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        public string GoogleMapUrl { get; set; }

        public Image Pictures { get; set; }
    }
}
