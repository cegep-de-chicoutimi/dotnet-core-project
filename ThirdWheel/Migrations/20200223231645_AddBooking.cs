﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ThirdWheel.Migrations
{
    public partial class AddBooking : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ImageUrl",
                table: "Bike");

            migrationBuilder.AddColumn<int>(
                name: "BikeId",
                table: "Image",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ImageId",
                table: "Contact",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Booking",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClientId = table.Column<string>(nullable: false),
                    BikeId = table.Column<int>(nullable: false),
                    PickUpTime = table.Column<DateTime>(nullable: false),
                    DropTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Booking", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Booking_Bike_BikeId",
                        column: x => x.BikeId,
                        principalTable: "Bike",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Booking_AspNetUsers_ClientId",
                        column: x => x.ClientId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Image_BikeId",
                table: "Image",
                column: "BikeId");

            migrationBuilder.CreateIndex(
                name: "IX_Contact_ImageId",
                table: "Contact",
                column: "ImageId");

            migrationBuilder.CreateIndex(
                name: "IX_Booking_BikeId",
                table: "Booking",
                column: "BikeId");

            migrationBuilder.CreateIndex(
                name: "IX_Booking_ClientId",
                table: "Booking",
                column: "ClientId");

            migrationBuilder.AddForeignKey(
                name: "FK_Contact_Image_ImageId",
                table: "Contact",
                column: "ImageId",
                principalTable: "Image",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Image_Bike_BikeId",
                table: "Image",
                column: "BikeId",
                principalTable: "Bike",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contact_Image_ImageId",
                table: "Contact");

            migrationBuilder.DropForeignKey(
                name: "FK_Image_Bike_BikeId",
                table: "Image");

            migrationBuilder.DropTable(
                name: "Booking");

            migrationBuilder.DropIndex(
                name: "IX_Image_BikeId",
                table: "Image");

            migrationBuilder.DropIndex(
                name: "IX_Contact_ImageId",
                table: "Contact");

            migrationBuilder.DropColumn(
                name: "BikeId",
                table: "Image");

            migrationBuilder.DropColumn(
                name: "ImageId",
                table: "Contact");

            migrationBuilder.AddColumn<string>(
                name: "ImageUrl",
                table: "Bike",
                nullable: true);
        }
    }
}
