﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ThirdWheel.Areas.Identity.Data;
using ThirdWheel.Models.Destinations;
using ThirdWheel.Models.Primitives;
using ThirdWheel.Models.Renting;

namespace ThirdWheel.Migrations
{
    public class ThirdWheelContext : IdentityDbContext<ThirdWheelUser>
    {
        public ThirdWheelContext(DbContextOptions<ThirdWheelContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Image>()
                .Property(carousel => carousel.Id)
                .IsRequired();

            builder.Entity<Bike>()
                .Property(bike => bike.Id)
                .IsRequired();

            builder.Entity<Owner>()
                .Property(owner => owner.Id)
                .IsRequired();

            builder.Entity<Contact>()
                .Property(contact => contact.Id)
                .IsRequired();

            builder.Entity<Site>()
                .Property(site => site.Id)
                .IsRequired();

            builder.Entity<Booking>()
                .Property(booking => booking.Id)
                .IsRequired();
        }

        public DbSet<ThirdWheel.Models.Renting.Bike> Bike { get; set; }

        public DbSet<ThirdWheel.Models.Renting.Contact> Contact { get; set; }

        public DbSet<ThirdWheel.Models.Renting.Owner> Owner { get; set; }

        public DbSet<ThirdWheel.Models.Primitives.Image> Image { get; set; }

        public DbSet<ThirdWheel.Models.Destinations.Site> Site { get; set; }
    }
}
